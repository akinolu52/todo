import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from "react-redux";
import { store } from './store';
import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';

const Todo = lazy(() => import('screens/Todo'));

function App(props) {

  return (
    <>
      <Provider store={store}>
        <Router>
          <Suspense fallback={<div>Loading...</div>}>
            <Switch>

              <Route exact={true} path="/" component={Todo} />

            </Switch>
          </Suspense>
        </Router>
      </Provider>
      <ReactNotification />
    </>
  );
}

export default App;
