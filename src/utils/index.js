import axios from 'axios';

export const _handleInputEnter = event => {
    if (event.keyCode === 13) {
        const form = event.target.form;
        const index = Array.prototype.indexOf.call(form, event.target);
        form.elements[index + 1].focus();
        event.preventDefault();
    }
}

export const _axios = axios.create({
    // 
    baseURL: `https://jsonplaceholder.typicode.com/`,
    // baseURL: `https://my-json-server.typicode.com/akinolu52/todo/`,
});

export const _isAnEmpytyObject = obj => {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

export const notification = {
    // showIcon: true,
    isMobile: true,
    width: 250,
    type: "info",
    insert: "top",
    container: "bottom-right",
    // --
    slidingEnter: {
        duration: 300,
        timingFunction: "linear",
        delay: 0
    },
    slidingExit: {
        duration: 300,
        timingFunction: "linear",
        delay: 0
    },
    touchRevert: {
        duration: 600,
        timingFunction: "linear",
        delay: 0
    },
    touchSlidingExit: {
        swipe: {
            duration: 600,
            timingFunction: "linear",
            delay: 0,
        },
        fade: {
            duration: 300,
            timingFunction: "linear",
            delay: 0
        }
    },
    // --
    dismiss: {
        duration: 5000,
        onScreen: true
    },
    animationIn: ["animated zoomIn"],
    animationOut: ["animated fadeOut"]
};