import * as types from "./types";
import { _axios, notification } from "../utils";
import { store as reduxStore } from '../store';
import { store } from 'react-notifications-component';


export const addTodo = (data) => dispatch => {
    dispatch({ type: types.ADD_TODO });
    const rStore = reduxStore.getState();
    let todos = rStore.todo.todos;
    // console.log(todos, data)
    return _axios({
        url: `todos`,
        method: 'post',
        data
    })
        .then(resp => {
            let data = resp.data;
            console.log(data)

            if (data && resp.status === 201) {
                todos = [data, ...todos];
                // delete (data.id)
                const todoPayload = {
                    todos,
                    loading: false,
                };
                console.log(todos)
                console.log('[todoPayload] ', todoPayload)
                dispatch({ type: types.ALL_TODO_SUCCESS, payload: todoPayload });

                store.addNotification({
                    ...notification,
                    title: 'Todo',
                    message: 'Todo added successfully!',
                    type: 'success'
                });

                return true;
            } else {
                store.addNotification({
                    ...notification,
                    title: 'Todo Error',
                    message: 'Error while adding todos',
                    type: 'danger'
                });

                // dispatch({ type: types.ALL_TODO_FAILURE, payload: errorDispatch });

                return false;
            }

        })
        .catch(e => {

            let title = "Todo Error!",
                type = "danger";

            if (e.response === undefined) {
                notification.message = 'No Internet Connection';
            } else {
                let status_code = e.response.status;
                if (status_code === 500 || status_code === 400) {
                    notification.message = e.response.data.message.toString() || 'An error occured, please try again';
                } else {
                    notification.message = e.response.data.message.toString()
                }
            }

            store.addNotification({
                ...notification,
                title, type
            });
            // dispatch({ type: types.REGISTER_ERROR, payload: errorDispatch });
            return false;
        });

};

export const allTodos = () => dispatch => {
    console.log('get//')
    dispatch({ type: types.ALL_TODO });
    return _axios({
        url: `todos`,
        method: 'get'
    })
        .then(resp => {
            let data = resp.data;
            console.log(resp)
            // dispatch({ type: types.ALL_TODO_SUCCESS, payload:  });

            if (data && resp.status === 200) {
                const todoPayload = {
                    todos: data,
                    loading: false
                };
                dispatch({ type: types.ALL_TODO_SUCCESS, payload: todoPayload });

                store.addNotification({
                    ...notification,
                    title: 'Todos',
                    message: 'Todos loaded successfully!',
                    type: 'success'
                });

                return true;
            } else {
                store.addNotification({
                    ...notification,
                    title: 'Todos Error',
                    message: 'Error while loading todos',
                    type: 'danger'
                });

                const errorDispatch = {
                    todos: [],
                    loading: false
                };

                dispatch({ type: types.ALL_TODO_FAILED, payload: errorDispatch });

                return false;
            }

        })
        .catch(e => {
            let title = "Todo Error!",
                type = "danger";

            if (e.response === undefined) {
                notification.message = 'No Internet Connection';
            } else {
                let status_code = e.response.status;
                if (status_code === 500 || status_code === 400) {
                    notification.message = e.response.data.message.toString() || 'An error occured, please try again';
                } else {
                    notification.message = e.response.data.message.toString()
                }
            }

            store.addNotification({
                ...notification,
                title, type
            });
            // dispatch({ type: types.REGISTER_ERROR, payload: errorDispatch });
            return false;
        });
};

export const updateTodo = (data) => dispatch => {
    console.log('updateTodo', `data`)
    dispatch({ type: types.UPDATE_TODO });
    return _axios({
        url: `todos/${data.todo.id}`,
        method: 'PUT',
        data
    })
        .then(resp => {
            let data = resp.data;
            console.log(resp)
            // dispatch({ type: types.UPDATE_TODO_SUCCESS, payload:  });

            if (data && resp.status === 200) {
                const todoPayload = {
                    todos: data,
                };
                dispatch({ type: types.UPDATE_TODO_SUCCESS, payload: todoPayload });

                store.addNotification({
                    ...notification,
                    title: 'Add comment',
                    message: 'comment added successfully!',
                    type: 'success'
                });

                return true;
            } else {
                store.addNotification({
                    ...notification,
                    title: 'Comment Error',
                    message: 'Error while loading todos',
                    type: 'danger'
                });

                const errorDispatch = {
                    todos: [],
                    loading: false
                };

                dispatch({ type: types.UPDATE_TODO_FAILED, payload: errorDispatch });

                return false;
            }

        })
        .catch(e => {
            console.log(e)
            let title = "Comment Error!",
                type = "danger";

            if (e.response === undefined) {
                notification.message = 'No Internet Connection';
            } else {
                let status_code = e.response.status;
                if (status_code === 500 || status_code === 400) {
                    notification.message = e.response.data.message.toString() || 'An error occured, please try again';
                } else {
                    notification.message = e.response.data.message.toString()
                }
            }

            store.addNotification({
                ...notification,
                title, type
            });
            // dispatch({ type: types.REGISTER_ERROR, payload: errorDispatch });
            return false;
        });
};

export const deleteTodo = (id) => dispatch => {
    dispatch({ type: types.DELETE_TODO });
    const rStore = reduxStore.getState();
    let todos = rStore.todo.todos;

    console.log(todos)
    return _axios({
        url: `todos/${id}`,
        method: 'DELETE',
    })
        .then(resp => {
            let data = resp.data;
            // console.log(resp)
            todos = todos.filter(todo => todo.id != id);
            console.log(todos)

            if (data && resp.status === 200) {
                const todoPayload = {
                    todos,
                    loading: false
                };
                dispatch({ type: types.DELETE_TODO_SUCCESS, payload: todoPayload });

                store.addNotification({
                    ...notification,
                    title: 'Delete todo',
                    message: 'todo added successfully!',
                    type: 'success'
                });

                return true;
            } else {
                store.addNotification({
                    ...notification,
                    title: 'Todo Error',
                    message: 'Error while loading todos',
                    type: 'danger'
                });

                const errorDispatch = {
                    todos: [],
                    loading: false
                };

                dispatch({ type: types.DELETE_TODO_FAILED, payload: errorDispatch });

                return false;
            }

        })
        .catch(e => {
            console.log(e)
            let title = "Todo Error!",
                type = "danger";

            if (e.response === undefined) {
                notification.message = 'No Internet Connection';
            } else {
                let status_code = e.response.status;
                if (status_code === 500 || status_code === 400) {
                    notification.message = e.response.data.message.toString() || 'An error occured, please try again';
                } else {
                    notification.message = e.response.data.message.toString()
                }
            }

            store.addNotification({
                ...notification,
                title, type
            });
            // dispatch({ type: types.REGISTER_ERROR, payload: errorDispatch });
            return false;
        });
};
