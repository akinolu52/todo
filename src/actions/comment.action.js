import * as types from "./types";
import { _axios, notification } from "../utils";
import { store as reduxStore } from '../store';
import { store } from 'react-notifications-component';


export const addComment = (data) => dispatch => {
    dispatch({ type: types.ADD_COMMENT });
    const rStore = reduxStore.getState();
    let comments = rStore.comment.comments;
    console.log(data)
    return _axios({
        url: `comments`,
        method: 'post',
        data
    })
        .then(resp => {
            let data = resp.data;
            console.log(data)

            if (data && resp.status === 201) {
                comments = [data, ...comments];
                // delete (data.id)
                const commentPayload = {
                    comments,
                    // : [data[0]],
                    loading: false,
                };
                dispatch({ type: types.ADD_COMMENT_SUCCESS, payload: commentPayload });

                store.addNotification({
                    ...notification,
                    title: 'Todo comment',
                    message: 'Todo comment added successfully!',
                    type: 'success'
                });

                return true;
            } else {
                store.addNotification({
                    ...notification,
                    title: 'Todo comment Error',
                    message: 'Error while adding todos',
                    type: 'danger'
                });

                // dispatch({ type: types.ADD_COMMENT_FAILURE, payload: errorDispatch });

                return false;
            }

        })
        .catch(e => {

            let title = "Todo Error!",
                type = "danger";

            if (e.response === undefined) {
                notification.message = 'No Internet Connection';
            } else {
                let status_code = e.response.status;
                if (status_code === 500 || status_code === 400) {
                    notification.message = e.response.data.message.toString() || 'An error occured, please try again';
                } else {
                    notification.message = e.response.data.message.toString()
                }
            }

            store.addNotification({
                ...notification,
                title, type
            });
            // dispatch({ type: types.REGISTER_ERROR, payload: errorDispatch });
            return false;
        });
};

export const allComments = (id) => dispatch => {
    console.log('get//')
    dispatch({ type: types.ALL_COMMENT });
    return _axios({
        url: `comments?postId=${id}`,
        method: 'get'
    })
        .then(resp => {
            let data = resp.data;
            console.log(resp)
            // dispatch({ type: types.ALL_COMMENT_SUCCESS, payload:  });

            if (data && resp.status === 200) {
                const commentPayload = {
                    comments: data,
                    loading: false
                };
                dispatch({ type: types.ADD_COMMENT_SUCCESS, payload: commentPayload });

                store.addNotification({
                    ...notification,
                    title: 'Todos comment',
                    message: 'Todos comment loaded successfully!',
                    type: 'success'
                });

                return true;
            } else {
                store.addNotification({
                    ...notification,
                    title: 'Todos comment Error',
                    message: 'Error while loading todos comments',
                    type: 'danger'
                });

                const errorDispatch = {
                    todos: [],
                    loading: false
                };

                dispatch({ type: types.ALL_COMMENT_FAILED, payload: errorDispatch });

                return false;
            }

        })
        .catch(e => {
            let title = "Todo Error!",
                type = "danger";

            if (e.response === undefined) {
                notification.message = 'No Internet Connection';
            } else {
                let status_code = e.response.status;
                if (status_code === 500 || status_code === 400) {
                    notification.message = e.response.data.message.toString() || 'An error occured, please try again';
                } else {
                    notification.message = e.response.data.message.toString()
                }
            }

            store.addNotification({
                ...notification,
                title, type
            });
            // dispatch({ type: types.REGISTER_ERROR, payload: errorDispatch });
            return false;
        });
};

export const deleteComment = (id) => dispatch => {
    dispatch({ type: types.DELETE_COMMENT });
    const rStore = reduxStore.getState();
    let comments = rStore.comment.comments;
    console.log(comments)
    return _axios({
        url: `comments/${id}`,
        method: 'DELETE',
    })
        .then(resp => {
            let data = resp.data;
            // console.log(resp)
            comments = comments.filter(comment => comment.id != id);
            console.log(comments)

            if (data && resp.status === 200) {
                const commentPayload = {
                    comments,
                };
                dispatch({ type: types.DELETE_COMMENT_SUCCESS, payload: commentPayload });

                store.addNotification({
                    ...notification,
                    title: 'Add comment',
                    message: 'comment added successfully!',
                    type: 'success'
                });

                return true;
            } else {
                store.addNotification({
                    ...notification,
                    title: 'Comment Error',
                    message: 'Error while loading todos',
                    type: 'danger'
                });

                const errorDispatch = {
                    todos: [],
                    loading: false
                };

                dispatch({ type: types.DELETE_COMMENT_FAILED, payload: errorDispatch });

                return false;
            }

        })
        .catch(e => {
            console.log(e)
            let title = "Comment Error!",
                type = "danger";

            if (e.response === undefined) {
                notification.message = 'No Internet Connection';
            } else {
                let status_code = e.response.status;
                if (status_code === 500 || status_code === 400) {
                    notification.message = e.response.data.message.toString() || 'An error occured, please try again';
                } else {
                    notification.message = e.response.data.message.toString()
                }
            }

            store.addNotification({
                ...notification,
                title, type
            });
            // dispatch({ type: types.REGISTER_ERROR, payload: errorDispatch });
            return false;
        });
};
