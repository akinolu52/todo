import * as types from "../actions/types";

const INITIAL_STATE = {
    comments: [],
    loading: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.ADD_COMMENT:
        case types.VIEW_COMMENT:
        case types.DELETE_COMMENT:
            return { ...state };

        case types.ADD_COMMENT_SUCCESS:
        case types.ADD_COMMENT_FAILED:
        case types.VIEW_COMMENT_SUCCESS:
        case types.VIEW_COMMENT_FAILED:
        case types.DELETE_COMMENT_SUCCESS:
        case types.DELETE_COMMENT_FAILED:
            console.log(action.payload)
            return { ...state, ...action.payload };
        default:
            return state;
    }
};