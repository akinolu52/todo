import { combineReducers } from 'redux';

import todo from './todo.reducer';
import comment from './comment.reducer';

const rootReducer = combineReducers({
    todo,
    comment,
});

export default rootReducer;