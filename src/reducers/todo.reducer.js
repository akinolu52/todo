import * as types from "../actions/types";

const INITIAL_STATE = {
    todos: [],
    // comment: [],
    loading: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.ADD_TODO:
        case types.ALL_TODO:
        case types.DELETE_TODO:
            return { ...state, loading: true };

        case types.ADD_TODO_SUCCESS:
        case types.ADD_TODO_FAILED:
        case types.ALL_TODO_SUCCESS:
        case types.ALL_TODO_FAILED:
        case types.DELETE_TODO_SUCCESS:
        case types.DELETE_TODO_FAILED:
            console.log('action.payload ', action.payload)
            return { ...state, ...action.payload };
        default:
            return state;
    }
};