import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import {
    allTodos, updateTodo, addTodo, deleteTodo,
    allComments, addComment, deleteComment
} from 'actions';
import { _isAnEmpytyObject } from 'utils';

function App(props) {
    const [count, setCount] = useState(201);
    const [title, setTitle] = useState('');
    const [comment, setComment] = useState('');
    const [todo, setTodo] = useState({});

    useEffect(() => {
        console.log(props.todos)
        // if (!props.todos) {
        //     console.log('get todos')
        allTodos();
        // } else {
        //     console.log('done')
        // }
    }, [])

    async function allTodos() {
        return props.allTodos();
    }

    function addTodo() {
        setCount(1 + count)
        console.log('todo ', title)
        const data = {
            userId: 10,
            id: count,
            title,
            completed: false,
            // comments: []
        };

        props.addTodo(data).then(() => setTitle(''));
    }

    function setActiveTodo(todo) {
        console.log('setActiveTodo -> ', todo)
        setTodo(todo);
        props.allComments(todo.id)
    }

    function addComment() {
        console.log('todo ', comment)
        // const commentData = [
        //     ...todo.comments,
        //     { body: comment }
        // ];
        // console.log(commentData);
        // todo.comments = commentData
        const data = {
            body: "vero eaque aliquid doloribus et culpa",
            email: "abc@xyz.com",
            id: props.comments.length + 1,
            name: comment,
            postId: todo.id
            // todo
        };
        console.log('todo ', data)

        props.addComment(data);
    }

    function deleteComment(id) {
        console.log(id)
        props.deleteComment(id);
    }

    function deleteTodo(id) {
        console.log(id)
        props.deleteTodo(id);
    }

    function handleEnter(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            if (comment) {
                addComment();
            } else {
                addTodo();
            }
        }
    }

    return (
        <div className="ui-flex uk-padding">
            <form onSubmit={addTodo}>
                <div className="uk-margin">
                    <label className="uk-form-label" htmlFor="form-stacked-text">Todo Title</label>
                    <div className="uk-form-controls">
                        <div class="uk-inline" style={{ width: "100%" }}>
                            {props.loadingTodo &&
                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: bolt"></span>
                            }
                            <input
                                autoFocus
                                className="uk-input"
                                onKeyDown={handleEnter}
                                onChange={event => setTitle(event.target.value)}
                                type="text"
                            />
                        </div>
                    </div>
                </div>
            </form>

            {!props.loadingTodo ? props.todos.length ? (
                <ul className="uk-list uk-list-striped">
                    {props.todos.map((todo, index) =>
                        <li key={index}>
                            <div className="uk-flex uk-flex-between">
                                <div>{todo.title}</div>
                                <div>
                                    {/*  */}
                                    <button className="uk-button uk-button-default" type="button" uk-toggle="target: #modal-example" style={{ border: "none", paddingRight: 0 }}>
                                        <span uk-icon="plus-circle" onClick={() => setActiveTodo(todo)} className="uk-margin-right"></span>
                                    </button>
                                    <a href="#" onClick={() => deleteTodo(todo.id)}>
                                        <span uk-icon="trash"></span>
                                    </a>
                                </div>
                            </div>
                        </li>
                    )}
                </ul>
            ) : (
                    <div className="uk-alert-primary" uk-alert="true">
                        <p>Please add todo.</p>
                    </div>
                ) : <div>Loading Todo...</div>}


            <div id="modal-example" uk-modal="true">
                <div className="uk-modal-dialog uk-modal-body">
                    {!_isAnEmpytyObject(todo) ? (
                        <>
                            <h2 className="uk-modal-title">'{todo.title}' comments</h2>
                            <div className="">
                                <form onSubmit={addComment}>
                                    <div className="uk-margin">
                                        <label className="uk-form-label" htmlFor="form-stacked-text">Todo comment</label>
                                        <div className="uk-form-controls">
                                            <div class="uk-inline" style={{ width: "100%" }}>
                                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: bolt"></span>
                                                <input
                                                    className="uk-input"
                                                    onKeyDown={handleEnter}
                                                    onChange={event => setComment(event.target.value)}
                                                    type="text"
                                                />
                                            </div>

                                        </div>
                                    </div>
                                </form>
                                {!props.loadingComment ? props.comments.length ? (
                                    <ul className="uk-list uk-list-striped">
                                        {props.comments.map((comment, index) =>
                                            <li key={index}>
                                                <div className="uk-flex uk-flex-between">
                                                    <div>{comment.name}</div>
                                                    <a onClick={() => deleteComment(comment.id)}>
                                                        <span uk-icon="trash"></span>
                                                    </a>
                                                </div>
                                            </li>
                                        )}
                                    </ul>
                                ) : (
                                        <div className="uk-alert-primary" uk-alert="true">
                                            <p>Please add comment to this todo.</p>
                                        </div>
                                    ) : <div>Loading comment...</div>}
                            </div>
                        </>
                    ) : null}
                </div>
            </div>

        </div>
    );
}

const mapStateToProps = ({ todo, comment }) => {
    const { todos, loading: loadingTodo } = todo;
    const { comments, loading: loadingComment } = comment;

    // console.log('todo ->>', todo, todos.length)
    // console.log('comment ->>', comment, comments.length)
    return { todos, comments, loadingComment, loadingTodo };
};

export default connect(
    mapStateToProps,
    {
        allTodos, addTodo, updateTodo, deleteTodo,
        allComments, addComment, deleteComment
    }
)(App);
